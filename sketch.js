var sally; // declare a variable called sally
function setup() {
    createCanvas(windowWidth, windowHeight);
    // https://p5js.org/reference/#/p5/colorMode
    colorMode(HSB, 360, 100, 100);

    // initialise the sally variable here
}

function draw() {
    background(100, 80, 50);
    // your "draw loop" code goes here
    
    // what function(s) do you need to call in here, and with what parameters?
}

function drawPokemon(aPokemon) {
    noStroke();
    fill(0);
    ellipse(aPokemon.x, aPokemon.y, 50, 50);
}

function updatePokemon(aPokemon) {
    // this is just a bit of controlled randomness to make the pokemon move around the canvas
    aPokemon.x += 0.05*width*cos(frameCount*0.01)*randomGaussian(0, 0.1);
    aPokemon.y += 0.05*height*cos(frameCount*0.011)*randomGaussian(0, 0.1);
}
